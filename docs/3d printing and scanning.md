#3d Pritning and Scanning










# Hinge

I started this project by creating a sketch and adding a rectangle with a square merged by the diameter at the end of the rectangle. I removed the excess lines and formed tangent constraints around the curved area of the shape. After I had that solidified and added two circles in the curved area of the shape. I also added some sketch dimensions to the circles.

![alt text](Screen_Shot_2022-03-28_at_2.21.46_PM.png)







 



For the inner layer I set a sketch dimension of 17.5 mm. And for the inner circle I inserted the formula D3 -2×.04. The .04 is for the tolerance in between the brace and the receiving slot and then rotates around the bar. 
![alt text](Screen_Shot_2022-03-10_at_12.02.11_PM.png)


Once I had the circles with the proper dimensions . I added a line to separate the rotating joint and the rest of the piece. From there I extruded the sketch by 3 mm. 


![alt text](Screen_Shot_2022-03-10_at_1.31.12_PM.png)


I selected the inner circle as well as the square made from the line that I separated the joint and the rest of the piece from. I extruded it by 10 mm and created a new body for peace. This Created 1/2 of male end of  the joint. 

![alt text](Screen_Shot_2022-03-10_at_1.23.18_PM.png)




I use that same mid plane,Then copied  the Body’s and combined them. Once I combined them I created a new component out of the new body.

![alt text](Screen_Shot_2022-03-17_at_3.37.09_PM.png)


For the female and I did the same corresponding process.

![alt text](Screen_Shot_2022-03-10_at_1.31.12_PM.png)
![alt text](Screen_Shot_2022-03-10_at_1.37.15_PM.png)
![alt text](Screen_Shot_2022-03-10_at_1.39.28_PM.png)



Once I had the second piece together. I went to the assembly menu and assembled a joint. For the motion of the joint I chose a revolute as I wanted this particular piece to Rotate At the center point. In order to get both pieces to align perfectly I also had to set the combine to in between two services. 
Four the male piece I selected the top and the bottom surface to be centered. For the female piece I selected the top and bottom surface of the bar and the slot with meat. And selected the inner wall to snap in place. 

![alt text](Screen_Shot_2022-03-10_at_2.16.08_PM.png)





Once I clicked OK and the pieces fit together and I need a demonstration to see how it will rotate. From there I saved it as an STL file and loaded it into a 3-D printer. The first generation and one side kind of miss printed. So I changed the orientation of the base and I’m waiting to see how it will turn out.

![alt text](Screen_Shot_2022-03-10_at_2.18.10_PM.png)






Here is the [link to the file.](https://a360.co/34zRfrT)




###Tweaks 

Since the results showed that there is a .5 mm offset in  thickness of a wall printed by the machine. for the inner space I want that wall to be 4 mm.I decided to add .5 mm to the thickness of the wall so that I will have An accurate dimension of 26 mm from the outer wall with an in a wall of 4 mm of depth. The inner walls in measure at 18 mm. I change the parameters to 4.5 for the Depth of the Br Ace. But I kept in mind that I actually wanted it to be at 4 mm so the added .5 will compensate for the distortion of the print quality.

![alt text](IMG_0877.jpg)  
![alt text](IMG_0876.jpg)

I also noticed originally I subtracted 3 mm instead of .3 mm from the clearance. So for the female sheath I used this equation
![alt text](Screen_Shot_2022-04-04_at_9.13.23_PM.png)
![alt text](IMG_0816_2.jpg)
![alt text](IMG_0815.jpg)





This time for the extrusion of the shaft length I want it to come out to be 10 mm so what I intend to do is subtract  of the outer brace  from the base of the outer wall at 9.5 mm. Which would leave me with 5 mm of extruded Space from the base of the wall. Seeing as though the thickness of the wall will Be subtracted from the extrusion. With a 5 mm extrusion I can mirror that and combine that would give and extruder chair for 10 mm.

![alt text](Screen_Shot_2022-04-04_at_9.13.36_PM.png)




I made sure to include the clearance as well as a parameter for the offset dimension just in case I need to use it later on in the design. I also added an equation for the air space in between the inner and outer shaft of 4 times the clearance. I entitled air space.

![alt text](Screen_Shot_2022-04-04_at_3.23.08_PM.png)





For the circumference of the inner shaft I also used an equation of the circumference of the outer shaft minus the air space. That way anytime I changed the circumference of the outer shaft the inner would also change accordingly. Without me having to go back in and re-configure the spaces.

![alt text](Screen_Shot_2022-04-04_at_3.29.22_PM.png)





you can view a link to the parameters data sheet [here.](https://docs.google.com/spreadsheets/d/1tFTlYgYkjUwptaDnO5hr3s5QRbJ5XcTm6RlhNnn4qOY/edit#gid=0)



There I created a rectangle with a parameter of the length and width that I entered In my Parameters. I then created a circle within the width of the perimeters and trimmed the inner lines to create a connected rounded curve at the end of the rectangle which will serve as the space for both the inner and outer connection 
Of the joint.

Then I created a Circle representing the space for the unsaved and entered in the parameters for that section.  There I created in the circle based off the equation I’m putting my parameters. I multiplied the clearance times 4 for the airspace. So that I would have 3 mm of space multiply by all four side of the circle  in between the inner shaft and the outer hole all the way around. To make sure that the space came out to be .6 mm I measure the distance on both sides from the top and the bottom they measure 6 mm. 

![alt text](Screen_Shot_2022-04-04_at_9.26.00_PM.png)







##Sidenote: 
for the next iteration I may add the offset space for the wall thickness of the outer layer as I’m not sure if that would factor in this world. I will see for the next round





When I took the tolerances to see that they equal to .6mm, which is the clearance times all four angles of the circumfrence.   I added a line to separate the joint area from the rest of the peace. And then extruded it out by the The brace settings
Then I created an extrusion off of half of the bracelength parameters  created a mirror plane to mirror the other side. 


![alt text](Screen_Shot_2022-04-04_at_4.06.16_PM.png)

![alt text](Screen_Shot_2022-04-04_at_7.21.48_PM.png)




I also check the tolerances in between the space to make sure that I had 10 mm which came out to be. Is there a separated the I made another permanent separate the US space from Between the  inner two. I added an extruited brace at the same with of the outer braces. And created a mid plane to mirror that on the other side.

![alt text](Screen_Shot_2022-04-04_at_4.29.01_PM.png)


![alt text](Screen_Shot_2022-04-04_at_4.31.38_PM.png)








From there are combined all the pieces and And saved it has a new component I also said the previous piece as a new component as well.
Then I assembled them at the centerpoint with the joint feature to see if they all fit in place properly.



![alt text](Screen_Shot_2022-04-04_at_8.29.45_PM.png)
![alt text](Screen_Shot_2022-04-04_at_4.57.52_PM.png)
![alt text](Screen_Shot_2022-04-04_at_8.34.08_PM.png)



##Sidenote: 

For this particular process is important to make sure that the joint motion feature is set to revolute to allow it to do a full 360 turn.




I also check the tolerance between the sheath of the female piece and the edge of the outer brace to make sure that the space was at at least. 3 mm, which is what I found.








##Reflections

*I now know that the printer that I used I can print a clearance at maximum at .3 mm anything beyond that will fuse the material.
 ![alt text](IMG_0881.jpg)

*I also know that when constructing the wall thicknesses I should factor in at least .5 mm of space to compensate for the offset the printer will print in.

* I said also make sure that when printing a joint that I do not put the supports setting to everywhere and instead allow the support setting to only be on the build plate.
![alt text](Screen_Shot_2022-04-05_at_11.27.25_AM.png). 
![alt text](Screen_Shot_2022-04-05_at_11.25.58_AM.png)
![alt text](IMG_0880.jpg)



you can find a link to the file [here.](https://a360.co/37iykmc)





















## Phogotgrametry

Photogrammetry 











​​I Purchased  a subscription to a photogrammetry app for the iPhone called PolyGram LIDAR and SCANNER .The idea is to capture an object and see if I can slice it and the CURA software I just got. I ran into a couple of issues during the process. The first was lighting the area that I didn’t have the best 
Lighting.
![alt text](Screen_Shot_2022-04-05_at_9.15.37_AM.png)
  





So I decided to take the pictures outside as the outside  would have the most ambience. I placed an object on a simple rotating platform And placed my phone on a tripod to hold it steady. 

![alt text](Screen_Shot_2022-04-05_at_9.15.50_AM.png)









While I took the pictures I rotated the platform and got about 21 images. The max amount in the program is 150 before the premium subscription.  
20 was the least you could have to get enough data to get the overall structure of the object.

![alt text](Screen_Shot_2022-04-05_at_9.15.58_AM.png)





 From there I uploaded it to my computer and then opened the object in Cura to slice. The model set the scale of the actual object. Which was 3.8 x 4.4 x 3.6 mm. After I sliced the object I tried to save it to my hard drive but it wouldn’t allow me to save it as a g code. I tried saving it as  an stl or gtlf .. still the same result. 

 ![alt text](Screen_Shot_2022-04-05_at_9.16.06_AM.png)









Software link 

https://apps.apple.com/us/app/polycam-lidar-3d-scanner/id1532482376


