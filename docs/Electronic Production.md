
# Electronics Production


## RGBPIR test simulation
For this particular board I decided to do a fusion between a PIR sensor and a RGBLED. I wasn’t completely sure how to go about this and I wanted to make sure before I did any building of the board that I had a code that would work so the first thing I did was go to tinker cad and create a mock simulation of what it could possibly look like in logic flow. I got an Arduino a Breboard connected a PIR sensor and a RGBLED to the board. For all the input output pans I connected them to the digital Ports on the Arduino Uno. From there I wrote a simple C code in Arduino format to see if I could create logic to make the PIR sensor send a signal to the RGB to flash all three colors whenever the sensor read HIGH, As well as turn the lights off when the sensor reads LOW. Once I did some configuring between the code and the devices it seemed to prove to work. 

![altt text](Screen_Shot_2022-04-10_at_10.34.33_PM.png)




## Datasheet

In the data sheet I looked up the chart for the 80 tiny 45 micro controller so I can figure out which ports I could use in place of the open port I needed for one of the devices. I decided to use port 3 since  in one of the RGB designs they were open. As for  port two I needed to use for the FTDI connection. Specifically the serial clock so that I can connect the micro controller to the PCU and they can communicate between each other.

![alt text](Screen_Shot_2022-04-10_at_11.47.09_PM.png)
![alt text](Screen_Shot_2022-04-11_at_12.04.21_AM.png)




## PCB DESIGN

From there From there I was ready to consider how I would go about composing the board. I looked at the schematic for both the RGBLED board as well as the PIR center. The first thing I noticed is that the port that were occupied by the RGB sensor left open the pin that was occupied to the input output terminal for the PIR sensor. So from there I composed this schematic. once  the schematic was completed I created the board in Eagle. From there connected all the traces in the board editing software in  Eagle. 



![allt text](Screen_Shot_2022-04-10_at_10.37.20_PM.png)









The first iteration of traces were too thin so I did it a second time and then went back and edited the PNG with some 2-D editing software to make the traces thicker. The post processing in the software was easy as all I had to do was fill in black and white for the spaces that I wanted to adjust, seeing as though the mods editing software recognizes black and white as the information needed to lay the Vector data. From there I added the according components which I really didn’t need to alter that much. The only thing I had to add was the PIR sensor. All the capacitors and resistors were connected to its proper ports based off of my conjecture.


![alt text](Screen_Shot_2022-04-10_at_10.41.01_PM.png)






## ATmelTest


I Stuff the board then connected to the programmer and got the greenlight confirmation that 
the connection was successful. 

![alt text](Screen_Shot_2022-04-10_at_10.42.54_PM.png)


Also looked to see if the computer The Microcontroller witch I did. From there It did.

![alt text](Screen_Shot_2022-04-10_at_10.45.44_PM.png)



## FILE SETUP

 From there I tried uploading a makefile from the PIR  registry. I just replaced the file name with the name I created. And created a C file for the code I did in the Arduino simulator:



 ![alt text](Screen_Shot_2022-04-10_at_10.49.46_PM.png)






## Revision

For this board I redesigned the pins and rerouted the blue pen to port PB three and put the SCK pin on the pin header to the RX pin on the FTDI connectors. I also altered the code for the accounting pin locations. From there I created a new design and a new makefile for the code. 


![alt text](Screen_Shot_2022-04-10_at_11.19.52_PM.png)

![altexrt ](Screen_Shot_2022-04-10_at_11.36.57_PM.png)






I think the problem in the first iteration was the original code was written in  C for Arduino which isn’t compatible within the syntax of standard C for the Microcontroller.So I gather the syntax from the standard C code in the original two boards and change the commands in the main link to turn on the LED for three colors. Hopefully this time everything comes together properly


![allt text](Screen_Shot_2022-04-10_at_11.06.30_PM.png)

![alt text](Screen_Shot_2022-04-10_at_11.07.20_PM.png)





Below is the revised code specified for the new board.

![alt text](Screen_Shot_2022-04-10_at_11.10.49_PM.png)

![alt text](Screen_Shot_2022-04-10_at_11.12.43_PM.png)
![alt text](Screen_Shot_2022-04-10_at_11.14.05_PM.png)










Once I finished soldering the Board i hooked it up to the programmer with a successful signal.







I tried flashing the code onto the board but I got  this error code. I'm not sure but i may have to do more alterations to the code



The last schematic had issues with the connection so I did one more design to have all the compliments connected to their corresponding port according to the defined ports in the C File. 















Unfortunately no success..

So I went and reviewed the code. I noticed the led port settings and the serial pot setting were the same. I’m not sure if this may be the issue but I will see if changing them to separates ports will fix the issue. 







