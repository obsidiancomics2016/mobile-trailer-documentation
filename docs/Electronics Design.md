

## Electornics design 


## Parts list 


* Push button switch 

* 1k resistor

* 1 ohm resistor 

* LED

* Arduino unno
 
 
## Software

TinkerCAD 

 







## Design Process 




I chose to simulate this design in TinkerCAd. In the main  menu I went to the “Create a new circuit'' main page and went to the components inventory. From there I selected a push button switch a LED and two resistors to be connected to the Arduino I selected as well. My intent is to connect both the resistor to the positive feed or the anode terminal on the resistor as well as the opposite into the cathode point from there I will connect the cathode to the ground and the anode feed to one of the positive ports on the opposite side.tyext
![alt text](Screen_Shot_2022-04-18_at_5.52.21_PM.png)




From there I connected ports Of the switch to the selected terminals of the Arduino. I created a line from terminal 1a and 1B to the ground. I then Connected terminal 2a and 2b to the positive ports on specifically the 3.3 V Port on the bottom side And the other end to one of the positive terminals from the other side of the Microcontroller.

![alt text](Screen_Shot_2022-04-18_at_5.56.23_PM.png)

![alt text](Screen_Shot_2022-04-18_at_5.56.52_PM.png)

I noticed that the light was turned on when I started the simulation button. When I turn the switch on I’m assuming that the gate would be left open so the current wouldn't be broken. When I press the switch again the gate closes which generates the current for the LED to turn on. I know the resistors taper the feed to get it to the proper current to engage the LED. However I was curious if I changed the ohms of the resistors to see if I could get the light brighter.

![alt text](Screen_Shot_2022-04-18_at_6.01.16_PM.png)
![alt txt](Screen_Shot_2022-04-18_at_6.02.05_PM.png)



 I increased  ohms to  and I noticed that the light would have shattered . So I set a  1 ohm resistor on one end and a one kilo ohm connected to the switch and the other two wires are connected to the anode  of the light got slightly brighter without cracking but it wasn’t significantly better than it was before when I used both 1K homes for both resistors.
 ![alt text](Screen_Shot_2022-04-18_at_5.59.09_PM.png)

# Results

once I had the components configured properly to where the button worked in alignment with the LED. They got the multimeter and tested the amperage the voltage as well as the resistance. To test the resistors I hope the positive a negative port to both ends of the resistors to get the average voltage and resistance. For the resistor connected to the LED I had to shut off the switch to get the resistance. However for the button I could leave it there and get the resistance. I am assuming since the button is receiving a constant feed of voltage while the LED is receiving voltage depending upon the configuration of the button. 

![alt text](Screen_Shot_2022-04-18_at_5.20.53_PM.png).  ![alt text](Screen_Shot_2022-04-18_at_5.23.16_PM.png)

![alt text](Screen_Shot_2022-04-18_at_5.15.02_PM.png). 


![alt text](Screen_Shot_2022-04-18_at_5.22.22_PM.png). ![alt text](Screen_Shot_2022-04-18_at_5.22.13_PM.png)

![alt text](Screen_Shot_2022-04-18_at_5.22.33_PM.png)


![alt text](Screen_Shot_2022-05-03_at_12.49.14_PM.png)









## Multimeter data

1k ohm resistor:

* Voltage =1.45
* Amperage= 177mA
* Resistance=1000

1 ohm Resistor:

* Voltage=1.45
* Amperage=1.46
* Resistance=1

below is a link to the simulation :

[link here](https://www.tinkercad.com/things/dX8Bz0xukrM-bodacious-wolt-kasi/editel?sharecode=HEaPMav4nyhjK19jagfvDb1vVIH9787VOmoptktFZ48)

[video](https://youtu.be/bxHGb9wKLWA)
