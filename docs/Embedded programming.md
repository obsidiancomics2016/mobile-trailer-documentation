

## Machine

 Roland SRM 20

# Testing 

I uploaded the file in mods. For the precision mail settings I zero the XY andZ for the origin as well as the Home settings I left the dog height at 2 mm.



![alt text](Screen_Shot_2022-04-15_at_10.58.47_AM.png)

For the PCB default settings when I use the 1/64 tool I made sure I use the mill traces selection. And when I use the 1/3 second to mail the outline I made sure that I select the 1/32 setting.



![alt text](Screen_Shot_2022-04-15_at_10.59.49_AM.png)
![alt text](Screen_Shot_2022-04-26_at_11.13.13_AM.png)

From there I Add the program calculate the black and white space in the PNG and convert them into vectors for the milling machine


![alt text](Screen_Shot_2022-04-15_at_11.01.51_AM.png)

I couldn’t find any end  mills at one 10th so I measured the 1/64 instead. Download the file from the FabAcademy repository. Load of the 1/64 end mill. Hi zeroed the XY and Z settings. And printed the traces as well as the interior. For the interior I use the 1/32 mill. I noticed it in between  1/16 th  and th 1/17 is the last two settings before the end milll fuses the traces together. So I now know that the traces can only be a 16th of an inch apart before they fused together.



![alt text](Screen_Shot_2022-04-15_at_11.47.36_AM.png)





## Design Process 


The board that I want to be programmed by my fabricated ISP will be a simple LED Programmed to give a blinking light. For this I’m using These components below.



# Components list :

* 10k resistor
* LED
* 1k resistor 


* ATtiny 45 microcontroller
* 1uF Capacitor 
* 2 x 3 pin Connector

# Tools 
 
 Roland SRM 20

 1/62 end milll
 1/32 end mill 

# Software

##  EagleCad

  



 I checked the DRC settings for the traces I set a 12 mm clearance for the wire and pads and 8 mm for the via. This should help with the auto router feature.

![al text](Screen_Shot_2022-04-25_at_11.44.38_AM.png)
![al text](Screen_Shot_2022-04-25_at_11.44.46_AM.png)
![al text](Screen_Shot_2022-04-25_at_11.40.35_AM.png)


For the board that I decided to have programmed I designed a simple LED with a ATtiny45 and a button switch.

I had the button connected to PB0, As well as the current limiting Resistor.since the button is giving output into the controller, i'm assuming the MOSI port will be sufficient.


![alt text](Screen_Shot_2022-04-25_at_3.09.29_PM.png)
I connected the The LED to the PB1 port on the micro controller as well as a resistor connected to the positive feed of the LED. as it will be displayin output form the mircocontroller.

![alt text](Screen_Shot_2022-04-25_at_3.09.54_PM.png)


The capacitor was also connected to the power of the market controller and the ground line to the Microcontroller.

![alt trext](Screen_Shot_2022-04-25_at_10.24.23_PM.png)

The pinheader which will serve as communication between the Microcontroller and the serial Port to the programmer or CPU. Had all the corresponding ports connected to the Microcontroller as well as a ground connection to the button.

![alt text](Screen_Shot_2022-04-25_at_10.30.19_PM.png)

## Schematic and Code 

Beolow is the schematic with a simple c code  and .makefile to trigger the LED when the button is pushed.I set the button as input and the LED as output,with the corresponding ports implemented from the schematic.

![alt text](Screen_Shot_2022-05-06_at_10.41.03_AM.png)  ![alt text](Screen_Shot_2022-05-06_at_11.09.22_AM.png)


![alt text](Screen_Shot_2022-05-06_at_10.43.15_AM.png)







After I came up with the schematic I used the auto router with the design rules I had already implemented based off of the test piece I printed off of the machine.

Here are the trace’s and initerrior 

![alt text](Screen_Shot_2022-04-26_at_9.33.18_AM.png)

![alt text](Screen_Shot_2022-04-26_at_9.33.38_AM.png)


## File processing

Once I had the Traces generated I removed all of the headings for each trace and just kept the Trace file and then exported it as a image so I could create the interior.

![alt text](Screen_Shot_2022-04-26_at_10.12.53_AM.png)

To create the interior I just used some basic illustrating software to create an outline of whit around the overall contour of the traces From there I created two separate files one containing the traces and the other containing the interior to be ran through mods.

## ISP Programmer 

I’m going to use Brian’s board, from a previous embedded programing  course, to program the button LED. I downloaded the trace and interior files from his page. You can find a link to how he designed [his board here.](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html) 

![alt text](Screen_Shot_2022-04-26_at_10.40.22_AM.png)

