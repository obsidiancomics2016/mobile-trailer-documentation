
# Output Devices



## Design Concept


For the output  device I decided to do a simple RGB LED. I downloaded the interior and trace file from the listing. Milled both traces and the interior according to the modifications in the  mods program. 

![alt text](Screen_Shot_2022-04-11_at_2.35.53_PM.png)



From there I took a look at the board and the components and gather them from the inventory and stuff to the components according to how I saw them placed on the board. I hooked the by the board up to the Atmel ICE  programmer to be flashed.





## Issues 

 I noticed that I got a red button indicating that something wasn’t right with the connection.  I looked at the board to make sure that I put all the components in properly compared it to what I saw and the schematic everything lined up but still the connection wasn’t reading  properly . I noticed that the 2x2 pinnhead didn’t have anything to connect to. Which served as a power source.At first I was going to see if I can find a mechanism to put together some batteries to hook up to the board but all I had were 9 V batteries and 1.5 batteriesAnd the only connections that I could find at the time were for the 9 V batteries. 

 ![alt txt](Screen_Shot_2022-04-11_at_2.36.12_PM.png)
 
 
 
 So from there I looked in the inventory and saw that we had individual FTDI cables. Then I decided to go and find a USB cable from the house that I knew I wouldn’t need. From there I cut the cord down the middle broke down the wires to see the color scheme and noticed that we had Corresponding cords in our inventory. So I got the corresponding cords inside of them to the cords that I found on the USB cord. 

 ![alt text](Screen_Shot_2022-04-11_at_2.36.29_PM.png)            ![alt text](Screen_Shot_2022-04-11_at_2.36.39_PM.png)
                                                        

![alt text](Screen_Shot_2022-04-11_at_2.36.46_PM.png)



Then reinforce them with rain tubing so none of the wires were touch. Once I had all the lines covered with shrink tubing I then covered the bundle of wires with them and foil. As that’s how the original casing was compiled and the cord .





## Conclusion
 From there I hooked up the red and the black wires to the positive and negative thoughts on the IV port pinhead and then hooked it up to the Atmel device and it seem to care of the connection. Once I saw that everything was connected to flash the board. By altering the change directories code and creating a make file folder in documents so the call can refer to the Make  file and flash the board. After that everything seem to work OK. There’s a video down below of the board Blinking


 [llink1](https://www.youtube.com/watch?v=H0vxfTRYKqw)

 [link 2](https://www.youtube.com/watch?v=TzTnn5sI5AU)
