# Computer aided cutting 




I wasn’t exactly sure the kerf  of the laser and how it would affect the cut so what I did was construct a kerf measuring tool. It’s a simple rectangle defined by these measurements. To create Progression of gaps that increased 8.25 mm. I used the equation below based off of the parameters that I keyed in for the test piece.






![alt text](Screen_Shot_2022-04-07_at_1.41.39_PM.png).    ![alt text](Screen_Shot_2022-04-07_at_1.49.14_PM.png)

![alt text](Screen_Shot_2022-04-07_at_1.50.12_PM.png)


 once I had the base format completed I impoted the file as a DXF and opened it in adobe illustator to be formmated for the laser engraver

![alt text](Screen_Shot_2022-04-08_at_11.14.44_AM.png)

   


I also found a finger jointed test kit on [thingiverse](https://www.thingiverse.com/thing:2854454/files) that I decided to try as well.
![alt text](Screen_Shot_2022-04-07_at_12.54.02_PM.png)


## Design rules 

## Kerf increase equation

*Slotwidth +  (Increase * A)

2.5+(.25*A)=

The A equaled how many times I wanted to add an increase to the hole of the slot for example if I wanted to have the slot increased by .50 mm A would equal 2



## Parameters 


 length:  2 inch
width: 1 inch
slot width inital: 2.5 mm .25m added as it increases in width


I also found a finger jointed test kit on the thingy verse that I decided to try as well.

## Tests
To test the kerf of the laser I created a 4in square and cut two squares in the middle of it. The idea behind this is that as the laser cuts the two squares in the middle I can slide those to the corner and measure the amount of space in between the edge of one square and the edge of the outer corner. I found that the kurf for the laser was about 2.6 mm

![alt text](IMG_1078.jpg)

![alt text](IMG_1017.jpg)

## Results 
I also  found that using the kerf test for the slots that the Tightest  joint fixture was at about 2.75 mm. So I made sure that the Slot of the width What is it that measurement.. Also made the length of the slot at a quarter of an inch.

![alt etxt](IMG_1020.jpg)

![alt text](IMG_1024.jpg)

![alt text](IMG_1023.jpg)


## Design process 


Before I go into the design I created some parameters for The length and width of The overall shape. I am thinking everything should be 1 inch that way if I needed to count the height I could just count the amount of pieces I see knowing that they’re exactly 1 inch. 
I may possibly able to compensate for the slot space however. I’m  used the information gathered from the Kerf test be before I solidify the holes for the pieces



![alt text](Screen_Shot_2022-04-11_at_2.16.09_PM.png)









From there I made a square based off of the parameters I said. I made four slots along the midpoints of the square as well as create a slot in the middle. Each the width of about  2.75 mm. With the intention for the ends to fit inside of each other snuggly.


![alt text](Screen_Shot_2022-04-12_at_10.38.23_AM.png)


![alt text](Screen_Shot_2022-04-12_at_10.36.57_AM.png)

![alt text](Screen_Shot_2022-04-12_at_11.22.09_AM.png)

Once I had the overall safe together I say export of the DXF into the Adobe software to create multiple layers of the file with inside adobe illustrator. 


Once I had the proper format for the file I went to the printing preferences for the EPILOG laser engraver.
 From the mat board settings I set the power at 100% and the speed at 80% and I had the frequency at 50. I set the bed of the board at 26 x 15“. As well as made sure that the cut settings or set to vector. 

![alt text](Screen_Shot_2022-04-12_at_2.34.20_AM.png)
 
 
 I had to do several passes for the cut to go all the way through but once the pieces were to the point where they would fall out automatically I took them out and cleaned them up.

 ![alt text](IMG_1076.jpg)

 When I was done I tested the rigidness of the joint  by connecting the inner pieces I saw that they had a relatively sn g clearance in between the joint space. I also measured the space in between the Joints  and saw that they came out to be at about 2.7 mm which was relatively close to what I set in the design.



![alt text](IMG_1073.jpg)

![alt text](IMG_1077.jpg)


![alt text](IMG_1074.jpg)
  
  From there I connected them to make a few different shapes.


![alt text](IMG_1075.jpg)

![alt text](IMG_1072.jpg)















## Issues


I had  an issue with accessing the proper plugin fro adobe illllustrator. for some resson its not pulling the proper laser engrave interface. im only seeing the menu for a standard ink priniter and not the laser engraver settings. I contatacted tech support and found that that I had the worng driver iintall for adobe. 

![alt text](Screen_Shot_2022-04-08_at_10.59.14_AM.png)

![allt text](Screen_Shot_2022-04-08_at_11.03.41_AM.png)


  I ended up havinig the wrong driver installled, so I replaced it with the proper driver and it work properly.  

  When I went to upload the DXF with the multiplied pres piece pieces the vectors were outside of the yard board. So what I did was go back into the original file at the dxf option sectiion, I made sure that I scaled the DXF at 100% with The scale compensation for each unit to be skilled at 1 inch.

![alt text](Screen_Shot_2022-04-11_at_2.06.18_PM.png)


![altt text](Screen_Shot_2022-04-11_at_2.08.46_PM.png)

  




\
